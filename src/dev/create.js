const create = JSON.parse({
    "status": "ok",
    "data": [
      {
        "mainKeys": [
          {
            "Phrase": "мтс работа отзывы сотрудников",
            "Shows": "",
            "checked": true
          },
          {
            "Phrase": "мтс работа отзывы клиентов",
            "Shows": "",
            "checked": true
          }
        ],
        "filteredKeys": [
          {
            "Shows": 31040,
            "Phrase": "мтс работа"
          },
          {
            "Shows": 1123,
            "Phrase": "работа +в мтс отзывы"
          },
          {
            "Shows": 283,
            "Phrase": "работа +в мтс удаленно отзывы"
          },
          {
            "Phrase": "работа +в мтс отзывы сотрудников",
            "Shows": 263
          },
          {
            "Phrase": "мтс работа +на дому отзывы",
            "Shows": 245
          },
          {
            "Phrase": "работа +в колл центре мтс отзывы",
            "Shows": 135
          },
          {
            "Shows": 127,
            "Phrase": "мтс удаленная работа отзывы сотрудников"
          },
          {
            "Shows": 104,
            "Phrase": "работа +в мтс удаленно +на дому отзывы"
          }
        ]
      }
    ],
    "taskID": 1816
  })

export { create }