const parser = JSON.parse({
    "мтс работа отзывы сотрудников": {
      "yandex": {
        "found": 4880,
        "results": {
          "1": {
            "url": "https://otzovik.com/reviews/rabota_v_mts/",
            "title": "Отзывы сотрудников: Работа в МТС",
            "passage": "Случайно нашла на отзовике отзывы о работе в МТС. Ну что ж поделюсь ка и я своим мнением. Работа вполне сносная. Зарплата белая. Работники получают дмс, которым мне пришлось несколько раз воспользоваться."
          },
          "2": {
            "url": "https://pravda-sotrudnikov.ru/company/mts-3",
            "title": "МТС: отзывы сотрудников о работодателе",
            "passage": "Нужны отзывы сотрудников о компании МТС? На нашем сайте есть информация о данной компании."
          },
          "3": {
            "url": "https://career.habr.com/companies/mts/scores/2022",
            "title": "Оценки и отзывы сотрудников о компании МТС... — Хабр Карьера",
            "passage": "Сейчас МТС - один из самых стабильных работодателей, который делает все возможное, чтобы обезопасить своих сотрудников и обеспечить комфортные условия труда."
          },
          "4": {
            "url": "https://nahjob.top/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D1%8F/%D0%BC%D1%82%D1%81",
            "title": "Работа в МТС | 448 отзывов сотрудников",
            "passage": "Ищете правдивые отзывы о МТС? Изучаете вакансии и зарплаты? Компания МТС, по статистике отзывов сотрудников нашего сайта, не является рекомендуемой к трудойстройству В списке 448 отзывов бывших работников про МТС."
          },
          "5": {
            "url": "https://DreamJob.ru/employers/25946",
            "title": "Работа в МТС ᐈ Отзывы сотрудников о работодателе МТС...",
            "passage": "Изучайте отзывы сотрудников о работе в компании МТС. Сотрудники компании МТС оценивают возможности карьерного роста на 3.8 из 5, на основании 7398 оценок."
          },
          "6": {
            "url": "https://dzen.ru/a/XXatzzXI2ACulHxE",
            "title": "Дорожит ли компания «МТС» своими сотрудниками? | Дзен",
            "passage": "В редакцию портала поступил ряд отзывов от работников компании МТС. Несмотря на то что организация давно ведет деятельность, большинство сотрудников – молодежь: студенты, молодые специалисты, кадры без опыта."
          },
          "7": {
            "url": "https://otrude.xyz/employers/25946",
            "title": "Работа в MTS ᐈ Отзывы сотрудников о работодателе... | Отруде",
            "passage": "Читайте отзывы сотрудников о работе в компании MTS (МТС) Узнайте об условиях труда Зарплате Руководстве И другие нюансы на сайте Отруде. Отзывы сотрудников о компании MTS."
          },
          "8": {
            "url": "https://www.woman.ru/psycho/career/thread/4206999/",
            "title": "Стоит ли идти работать в мтс? - 60 ответов на форуме Woman.ru...",
            "passage": "Я работаю в мтс. Но не в салоне. первые 2.5 года в колцентре, сейчас в отделе претензий. работа нормальная. зарплату вовремя платят."
          },
          "9": {
            "url": "https://vk.com/wall-139177113_477791",
            "title": "Думал пойти оператором контактного центра МТС. Подскажите...",
            "passage": "Я пишу про колл-центр МТС по работе с массовым сегментом. Там звонки поступают хаотичным способом, любому оператору любой звонок. И днем перерыв между звонками,20 секунд, а ночью-1,5 минуты."
          },
          "10": {
            "url": "https://www.Rabota.ru/company/mts13/feedback/",
            "title": "Работа в МТС - отзывы сотрудников в Москве",
            "passage": "Отзывы о работе в МТС в Москве. Узнайте об условиях труда, зарплате, руководстве."
          }
        }
      },
      "google": {
        "date": "20230915T165224",
        "found": 3790000,
        "page": 0,
        "first": 1,
        "last": 10,
        "results": {
          "1": {
            "url": "https://pravda-sotrudnikov.ru/company/mts-3",
            "title": "МТС: отзывы сотрудников о работодателе",
            "contenttype": "organic",
            "breadcrumbs": "https://pravda-sotrudnikov.ru › company › mts-3",
            "stars": "Рейтинг: 2,9",
            "passage": "Минусы конечно есть в работе, это все-таки работа, а не курорт. Но для меня плюсы в работе МТС однозначно перевешивают. Во-первых, зарплата. Все мы работаем ...",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://pravda-sotrudnikov.ru/company/mts-3"
          },
          "2": {
            "url": "https://otzovik.com/reviews/rabota_v_mts/",
            "title": "Отзывы сотрудников: Работа в МТС",
            "contenttype": "organic",
            "breadcrumbs": "https://otzovik.com › reviews › rabota_v_mts",
            "stars": "Рейтинг: 4",
            "passage": "График. Нервно, не все выдержат. Случайно нашла на отзовике отзывы о работе в МТС. Ну что ж поделюсь ка и я своим мнением. Работа вполне сносная. Зарплата белая ...",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://otzovik.com/reviews/rabota_v_mts/"
          },
          "3": {
            "url": "https://dreamjob.ru/employers/25946",
            "title": "Работа в МТС - Отзывы сотрудников - Dream Job",
            "contenttype": "organic",
            "breadcrumbs": "https://dreamjob.ru › ... › Отзывы сотрудников ✅",
            "stars": "Рейтинг: 4",
            "passage": "Изучайте отзывы сотрудников о работе в компании МТС. Будьте в курсе условий труда, зарплат, карьерных возможностей благодаря сервису Dream Job.",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://dreamjob.ru/employers/25946"
          },
          "4": {
            "url": "https://career.habr.com/companies/mts/scores",
            "title": "Оценки и отзывы сотрудников о компании МТС за 2023 год",
            "contenttype": "organic",
            "breadcrumbs": "https://career.habr.com › companies › mts › scores",
            "passage": "Пока только 54 сотрудника дали оценку компании и 32 из них оставили комментарии. Для демонстрации полученной суммарной оценки нужно собрать минимум 75 оценок. 1 ...",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://career.habr.com/companies/mts/scores"
          },
          "5": {
            "url": "https://nahjob.top/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D1%8F/%D0%BC%D1%82%D1%81",
            "title": "Работа в МТС | 448 отзывов сотрудников",
            "contenttype": "organic",
            "breadcrumbs": "https://nahjob.top › Худшие компании ⚫",
            "passage": "Работа в МТС: отзывы сотрудников. Ищете правдивые отзывы о МТС? Изучаете вакансии и зарплаты? Компания МТС, по статистике отзывов сотрудников нашего сайта, ...",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://nahjob.top/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D1%8F/%D0%BC%D1%82%D1%81"
          },
          "6": {
            "url": "https://otzyvy-mobile.ru/mts/otzyvy-sotrudnikov/",
            "title": "МТС Отзывы Сотрудников",
            "contenttype": "organic",
            "breadcrumbs": "https://otzyvy-mobile.ru › МТС",
            "stars": "Рейтинг: 4,9",
            "passage": "Я IT-шником в МТС устроился полгода назад. Быстро привык, работа нормальная, требования вполне ожидаемые, каких- то мегасложных заданий не дают, сроки тоже ...",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://otzyvy-mobile.ru/mts/otzyvy-sotrudnikov/"
          },
          "7": {
            "url": "https://tipworker.com/otzyvy-sotrudnikov/mts",
            "title": "МТС ᐈ отзывы сотрудников о работе в компании 2022- ...",
            "contenttype": "organic",
            "breadcrumbs": "https://tipworker.com › otzyvy-sotrudnikov › mts",
            "stars": "Рейтинг: 3,5",
            "passage": "Интересует работа в МТС? У нас есть отзывы сотрудников о работодателе ПАО МТС 2022-2023 года.",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://tipworker.com/otzyvy-sotrudnikov/mts"
          },
          "8": {
            "url": "https://govorimorabote.ru/mts-otzyvy-sotrudnikov.html",
            "title": "МТС - отзывы сотрудников",
            "contenttype": "organic",
            "breadcrumbs": "https://govorimorabote.ru › mts-otzyvy-sotrudnikov",
            "passage": "",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://govorimorabote.ru/mts-otzyvy-sotrudnikov.html"
          },
          "9": {
            "url": "https://trudogolic.ru/provajdery-svyazi/mts",
            "title": "МТС: отзывы сотрудников о работодателе и клиентов ...",
            "contenttype": "organic",
            "breadcrumbs": "https://trudogolic.ru › provajdery-svyazi › mts",
            "passage": "Пошла в МТС из-за белой зарплаты и близости к дому. И сама удивилась, насколько мне зашла эта работа. Мне нравится работать с людьми и продавать. Коллектив ...",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://trudogolic.ru/provajdery-svyazi/mts"
          },
          "10": {
            "url": "https://rabota.kitabi.ru/otzyvy-sotrudnikov/mts",
            "title": "отзывы сотрудников о работе в компании ПАО МТС - Kitabi.ru",
            "contenttype": "organic",
            "breadcrumbs": "https://rabota.kitabi.ru › otzyvy-sotrudnikov › mts",
            "stars": "Рейтинг: 62%",
            "passage": "Интересует работа в компании МТС? У нас есть отзывы сотрудников о работодателе ПАО МТС в 2020 году - узнай зарплаты в Москве и СПБ.",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://rabota.kitabi.ru/otzyvy-sotrudnikov/mts"
          }
        }
      }
    },
    "мтс работа отзывы клиентов": {
      "yandex": {
        "found": 6866,
        "results": {
          "1": {
            "url": "https://otzovik.com/reviews/rabota_v_mts/",
            "title": "Отзывы сотрудников: Работа в МТС",
            "passage": "Работа в МТС - отзывы сотрудников. Работаю в МТС уже год, в розничной сети специалистом по продажам."
          },
          "2": {
            "url": "https://nahjob.top/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D1%8F/%D0%BC%D1%82%D1%81",
            "title": "Работа в МТС | 448 отзывов сотрудников",
            "passage": "Отзывы сотрудников о работе в МТС 448 отзывов сотрудников. Работа в МТС: отзывы сотрудников."
          },
          "3": {
            "url": "https://dzen.ru/a/ZI7eCWSgag-fFq6p",
            "title": "Отзыв о работе в &quot;МТС&quot;. Рассказываю, как все было. | Дзен",
            "passage": "В предыдущей статье про удаленку я пообещала рассказать о своем опыте работы в МТС. Я не преследую никаких целей в своем отзыве, лишь расскажу о своих впечатлениях предельно честно."
          },
          "4": {
            "url": "https://vk.com/wall-139177113_477791",
            "title": "Думал пойти оператором контактного центра МТС. Подскажите...",
            "passage": "Там запрещено контактировать с клиентами. Работала в мтс диспетчером 2 месяца и сбежала от туда."
          },
          "5": {
            "url": "https://career.habr.com/companies/mts/scores/2022",
            "title": "Оценки и отзывы сотрудников о компании МТС... — Хабр Карьера",
            "passage": "В любых коммуникациях и в любой работе - главное - клиент. Супер рада здесь работать - люди здесь осознанные и думающие, думаю это из-за того, что наш президент крутой, от него же набираются такие же люди "
          },
          "6": {
            "url": "https://pravda-sotrudnikov.ru/company/mts-3",
            "title": "МТС: отзывы сотрудников о работодателе",
            "passage": "Нужны отзывы сотрудников о компании МТС? На нашем сайте есть информация о данной компании."
          },
          "7": {
            "url": "https://DreamJob.ru/employers/25946",
            "title": "Работа в МТС ᐈ Отзывы сотрудников о работодателе МТС...",
            "passage": "Изучайте отзывы сотрудников о работе в компании МТС."
          },
          "8": {
            "url": "https://www.otzyvru.com/mts",
            "title": "МТС отзывы - ответы от официального представителя - Первый...",
            "passage": "Отзывы о МТС. Бренд МТС стоимостью $9,7 миллиарда третий год подряд признан самым дорогим российским брендом..."
          },
          "9": {
            "url": "https://otrude.xyz/employers/25946",
            "title": "Работа в MTS ᐈ Отзывы сотрудников о работодателе... | Отруде",
            "passage": "Читайте отзывы сотрудников о работе в компании MTS (МТС) Узнайте об условиях труда Зарплате Руководстве И другие нюансы на сайте Отруде. Отзывы сотрудников о компании MTS."
          },
          "10": {
            "url": "https://otvet.mail.ru/question/211112224",
            "title": "Ищу работу. Есть сотрудники МТС? Напишите отзывы о работе",
            "passage": "Я уже пять лет работаю в этой компании, и даже пару раз оставляла отзывы об МТС на студенческих форумах."
          }
        }
      },
      "google": {
        "date": "20230915T165225",
        "found": 2620000,
        "page": 0,
        "first": 1,
        "last": 9,
        "results": {
          "1": {
            "url": "https://pravda-sotrudnikov.ru/company/mts-3",
            "title": "МТС: отзывы сотрудников о работодателе",
            "contenttype": "organic",
            "breadcrumbs": "https://pravda-sotrudnikov.ru › company › mts-3",
            "stars": "Рейтинг: 2,9",
            "passage": "Минусы конечно есть в работе, это все-таки работа, а не курорт. Но для меня плюсы в работе МТС однозначно перевешивают. Во-первых, зарплата. Все мы работаем ...",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://pravda-sotrudnikov.ru/company/mts-3"
          },
          "2": {
            "url": "https://otzovik.com/reviews/rabota_v_mts/",
            "title": "Отзывы сотрудников: Работа в МТС",
            "contenttype": "organic",
            "breadcrumbs": "https://otzovik.com › reviews › rabota_v_mts",
            "stars": "Рейтинг: 4",
            "passage": "Работа в МТС - отзывы сотрудников. Рекомендуют 76%. Отзыв о Работа в МТС. Зарплата. Надежность. Отношение к персоналу. Социальный пакет. Условия труда. Добавить ...",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://otzovik.com/reviews/rabota_v_mts/"
          },
          "3": {
            "url": "https://dreamjob.ru/employers/25946",
            "title": "Работа в МТС - Отзывы сотрудников - Dream Job",
            "contenttype": "organic",
            "breadcrumbs": "https://dreamjob.ru › ... › Отзывы сотрудников ✅",
            "stars": "Рейтинг: 4",
            "passage": "Рекомендуем изучить минимум 5-7 отзывов о работодателе МТС. Это поможет составить правильное впечатление о компании. Руководитель группы обслуживания клиентов.",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://dreamjob.ru/employers/25946"
          },
          "4": {
            "url": "https://otzyvy-mobile.ru/mts/otzyvy-sotrudnikov/",
            "title": "МТС Отзывы Сотрудников",
            "contenttype": "organic",
            "breadcrumbs": "https://otzyvy-mobile.ru › МТС",
            "stars": "Рейтинг: 4,9",
            "passage": "Мне работа в МТС не сразу понравилась – думала, никогда не выучу это все, к клиентам подойти стеснялась. Но ради официального трудоустройства решила держаться и ...",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://otzyvy-mobile.ru/mts/otzyvy-sotrudnikov/"
          },
          "5": {
            "url": "https://career.habr.com/companies/mts/scores",
            "title": "Оценки и отзывы сотрудников о компании МТС за 2023 год",
            "contenttype": "organic",
            "breadcrumbs": "https://career.habr.com › companies › mts › scores",
            "stars": "Рейтинг: 4,4",
            "passage": "Меня порадовали стабильные выплаты заработной платы и хорошие условия для работы. Недостатки. Из недостатков только один - не хватает сотрудников на весь ...",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://career.habr.com/companies/mts/scores"
          },
          "6": {
            "url": "https://tipworker.com/otzyvy-sotrudnikov/mts",
            "title": "МТС ᐈ отзывы сотрудников о работе в компании 2022- ...",
            "contenttype": "organic",
            "breadcrumbs": "https://tipworker.com › otzyvy-sotrudnikov › mts",
            "stars": "Рейтинг: 3,5",
            "passage": "Интересует работа в МТС? У нас есть отзывы сотрудников о работодателе ПАО МТС 2022-2023 года.",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://tipworker.com/otzyvy-sotrudnikov/mts"
          },
          "7": {
            "url": "https://nahjob.top/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D1%8F/%D0%BC%D1%82%D1%81",
            "title": "Работа в МТС | 448 отзывов сотрудников",
            "contenttype": "organic",
            "breadcrumbs": "https://nahjob.top › Худшие компании ⚫",
            "passage": "Советуем познакомиться с отзывами. Изучите отзывы тех кто работал, чтобы узнать реальную зарплату в МТС, плюсы и минусы работы и другие условия в МТС. На нашем ...",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://nahjob.top/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D1%8F/%D0%BC%D1%82%D1%81"
          },
          "8": {
            "url": "https://rabota.kitabi.ru/otzyvy-sotrudnikov/mts",
            "title": "отзывы сотрудников о работе в компании ПАО МТС - Kitabi.ru",
            "contenttype": "organic",
            "breadcrumbs": "https://rabota.kitabi.ru › otzyvy-sotrudnikov › mts",
            "stars": "Рейтинг: 62%",
            "passage": "Интересует работа в компании МТС? У нас есть отзывы сотрудников о работодателе ПАО МТС в 2020 году - узнай зарплаты в Москве и СПБ.",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://rabota.kitabi.ru/otzyvy-sotrudnikov/mts"
          },
          "9": {
            "url": "https://www.otzyvru.com/mts/otzyvy-sotrudnikov",
            "title": "МТС отзывы сотрудников, отзывы о работе в МТС",
            "contenttype": "organic",
            "breadcrumbs": "https://www.otzyvru.com › mts › otzyvy-sotrudnikov",
            "passage": "МТС отзывы сотрудников о работе в МТС, отзывы о работодателе. Зарплата, условия труда, карьерный рост.",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://www.otzyvru.com/mts/otzyvy-sotrudnikov"
          }
        }
      }
    },
    "мтс работа": {
      "yandex": {
        "found": 10334,
        "results": {
          "1": {
            "url": "https://hh.ru/employer/3776",
            "title": "Вакансии компании МТС - работа в Москве, Нижнем Новгороде...",
            "passage": "Работа в компании МТС. Информация о компании и все открытые вакансии в Москве, Нижнем Новгороде, Краснодаре, Новосибирске."
          },
          "2": {
            "url": "https://otzovik.com/reviews/rabota_v_mts/",
            "title": "Отзывы сотрудников: Работа в МТС",
            "passage": "Всем привет! Сегодня я решила рассказать вам про работу в салоне связи МТС. Если вы целеустремленный, амбициозный и любите поболтать, то вам сюда."
          },
          "3": {
            "url": "https://job-mtsretail.ru/",
            "title": "Вакансии и работа в МТС",
            "passage": "Стань частью команды! Вакансии и работа в МТС: Ознакомься с горящими вакансиями или найди подходящую именно для себя."
          },
          "4": {
            "url": "https://job.mtsbank.ru/",
            "title": "Работа в МТС банке | Головной офис",
            "passage": "Специальные тарифы на мобильную связь и предложения от группы компаний МТС. Тренировки с MTS Fintech Run club, безлимитный тренажерный зал."
          },
          "5": {
            "url": "https://mts.ai/ru/career/",
            "title": "Карьера в компании MTS AI - Вакансии",
            "passage": "Карьерные возможности в компании МТС АИ: мы создаем все условия для комфортной работы и развития. Работа в команде профессионалов со всего мира."
          },
          "6": {
            "url": "https://career.habr.com/companies/mts/scores/2022",
            "title": "Оценки и отзывы сотрудников о компании МТС за... — Хабр Карьера",
            "passage": "Сейчас МТС - один из самых стабильных работодателей, который делает все возможное, чтобы обезопасить своих сотрудников и обеспечить комфортные условия труда."
          },
          "7": {
            "url": "https://Russia.SuperJob.ru/clients/mts-81084/vacancies.html",
            "title": "Вакансии МТС в России, работа в МТС на Superjob",
            "passage": "Удаленная работа. Работать с клиентами МТС-Банка, имеющими просроченную задолженность. Оформлять кредитные каникулы для снижения финансовой…"
          },
          "8": {
            "url": "https://dzen.ru/a/XXatzzXI2ACulHxE",
            "title": "Дорожит ли компания «МТС» своими сотрудниками? | Дзен",
            "passage": "В редакцию портала поступил ряд отзывов от работников компании МТС. На работу заставляют приходить за 30 минут до официального начала смены и уходить так же."
          },
          "9": {
            "url": "https://t.me/job_mts",
            "title": "Telegram: Contact @job_mts",
            "passage": "Вакансии МТС в IT и digital. Онлайн, бесплатно и без регистрации, конечно."
          },
          "10": {
            "url": "https://www.Rabota.ru/company/mts13/vacancies/",
            "title": "Вакансии МТС в Москве - поиск работы на rabota.ru",
            "passage": "Работа в МТС в Москве. Персональная лента рекомендаций и работа для всех: студентов, фрилансеров, пенсионеров, людей с особенностями здоровья и др."
          }
        }
      },
      "google": {
        "date": "20230915T165226",
        "found": 0,
        "page": 0,
        "first": 1,
        "last": 6,
        "results": {
          "1": {
            "url": "https://job.mts.ru/",
            "title": "Вакансии - МТС",
            "contenttype": "organic",
            "breadcrumbs": "https://job.mts.ru",
            "passage": "Информация об этой странице недоступна.Подробнее…",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://job.mts.ru/"
          },
          "2": {
            "url": "https://hh.ru/employer/3776",
            "title": "Вакансии компании МТС - работа в Москве, Нижнем ...",
            "contenttype": "organic",
            "breadcrumbs": "https://hh.ru › employer",
            "passage": "Работа в компании МТС. Информация о компании и все открытые вакансии в Москве, Нижнем Новгороде, Краснодаре, Санкт-Петербурге.",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://hh.ru/employer/3776"
          },
          "3": {
            "url": "https://www.superjob.ru/clients/mts-81084.html",
            "title": "Работа в компании МТС в Москве, вакансии ...",
            "contenttype": "organic",
            "breadcrumbs": "https://www.superjob.ru › clients › mts-81084",
            "passage": "Специалист по работе с клиентами МТС (удаленная работа). 48 000 — 60 000 ₽/месяц. МТС · ПАО &quot;МТС&quot;. Москва. Охотный ряд. Отклик без резюме. Опыт не нужен.",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://www.superjob.ru/clients/mts-81084.html"
          },
          "4": {
            "url": "https://otzovik.com/reviews/rabota_v_mts/",
            "title": "Отзывы сотрудников: Работа в МТС",
            "contenttype": "organic",
            "breadcrumbs": "https://otzovik.com › reviews › rabota_v_mts",
            "stars": "Рейтинг: 4",
            "passage": "График. Нервно, не все выдержат. Случайно нашла на отзовике отзывы о работе в МТС. Ну что ж поделюсь ка и я своим мнением. Работа вполне сносная. Зарплата белая ...",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://otzovik.com/reviews/rabota_v_mts/"
          },
          "5": {
            "url": "https://career.habr.com/companies/mts/vacancies",
            "title": "Открытые вакансии компании МТС - Хабр Карьера",
            "contenttype": "organic",
            "breadcrumbs": "https://career.habr.com › companies › mts › vacancies",
            "passage": "234 открытых вакансии компании «МТС» на Хабр Карьере.",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://career.habr.com/companies/mts/vacancies"
          },
          "6": {
            "url": "https://job.mtsbank.ru/rabota-s-klientami/",
            "title": "Работа с клиентами - Работа в МТС банке",
            "contenttype": "organic",
            "breadcrumbs": "https://job.mtsbank.ru › rabota-s-klientami",
            "passage": "",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://job.mtsbank.ru/rabota-s-klientami/"
          }
        }
      }
    },
    "работа +в мтс отзывы": {
      "yandex": {
        "found": 4875,
        "results": {
          "1": {
            "url": "https://otzovik.com/reviews/rabota_v_mts/",
            "title": "Отзывы сотрудников: Работа в МТС",
            "passage": "Хочу поделиться с Вами отзывом, а заодно и оптом работы в МТС, было это ее в студенческие годы, не так давно. Случайно нашла на отзовике отзывы о работе в МТС."
          },
          "2": {
            "url": "https://DreamJob.ru/employers/25946",
            "title": "Работа в МТС ᐈ Отзывы сотрудников о работодателе МТС...",
            "passage": "Изучайте отзывы сотрудников о работе в компании МТС. Будьте в курсе условий труда, зарплат, карьерных возможностей благодаря сервису Dream Job."
          },
          "3": {
            "url": "https://career.habr.com/companies/mts/scores/2022",
            "title": "Оценки и отзывы сотрудников о компании МТС... — Хабр Карьера",
            "passage": "Хотите спокойную работу без авралов и спешки - вас ждёт Мой МТС (50% команд, остальные 50% не рекомендую) или Smart Pet/Smart Wearable."
          },
          "4": {
            "url": "https://pravda-sotrudnikov.ru/company/mts-3",
            "title": "МТС: отзывы сотрудников о работодателе",
            "passage": "Нужны отзывы сотрудников о компании МТС? На нашем сайте есть информация о данной компании. Оказание услуг, Провайдеры и связь. Написать отзыв про МТС."
          },
          "5": {
            "url": "https://nahjob.top/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D1%8F/%D0%BC%D1%82%D1%81",
            "title": "Работа в МТС | 448 отзывов сотрудников",
            "passage": "Работа в МТС: отзывы сотрудников. При поиске работы лучше заранее прочитать отзывы про МТС, чтобы понять стоит ли работать в МТС."
          },
          "6": {
            "url": "https://www.woman.ru/psycho/career/thread/4206999/",
            "title": "Стоит ли идти работать в мтс? - 60 ответов на форуме Woman.ru...",
            "passage": "Так что в МТС в плане работы как опыта можно спокойно работать, но будьте готовы что вам будут мозги долбить каждый день по плану, я не обращаю внимания вообще на это."
          },
          "7": {
            "url": "https://dzen.ru/a/ZI7eCWSgag-fFq6p",
            "title": "Отзыв о работе в &quot;МТС&quot;. Рассказываю, как все было. | Дзен",
            "passage": "В предыдущей статье про удаленку я пообещала рассказать о своем опыте работы в МТС. Самой привлекательной вакансией мне показалась именно от МТС."
          },
          "8": {
            "url": "https://pikabu.ru/story/kak_ya_rabotal_v_mts_6078582",
            "title": "Как я работал в МТС. | Пикабу",
            "passage": "Как я работал в МТС. — пост пикабушника ksenobianinSanta. Комментариев - 356, сохранений - 219. Присоединяйтесь к обсуждению или опубликуйте свой пост!"
          },
          "9": {
            "url": "https://otvet.mail.ru/question/211112224",
            "title": "Ищу работу. Есть сотрудники МТС? Напишите отзывы о работе",
            "passage": "Я тут тоже недавно отзывы об МТС в Интернете лопатила. Меня привлекли дополнительные бонусы - сотрудники салонов получают скидки на связь."
          },
          "10": {
            "url": "https://otrude.xyz/employers/25946",
            "title": "Работа в MTS ᐈ Отзывы сотрудников о работодателе... | Отруде",
            "passage": "Читайте отзывы сотрудников о работе в компании MTS (МТС) Узнайте об условиях труда Зарплате Руководстве И...Отзывы сотрудников о компании MTS."
          }
        }
      },
      "google": {
        "date": "20230915T165226",
        "found": 4550000,
        "page": 0,
        "first": 1,
        "last": 10,
        "results": {
          "1": {
            "url": "https://otzovik.com/reviews/rabota_v_mts/",
            "title": "Отзывы сотрудников: Работа в МТС",
            "contenttype": "organic",
            "breadcrumbs": "https://otzovik.com › reviews › rabota_v_mts",
            "stars": "Рейтинг: 4",
            "passage": "График. Нервно, не все выдержат. Случайно нашла на отзовике отзывы о работе в МТС. Ну что ж поделюсь ка и я своим мнением. Работа вполне сносная. Зарплата белая ...",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://otzovik.com/reviews/rabota_v_mts/"
          },
          "2": {
            "url": "https://pravda-sotrudnikov.ru/company/mts-3",
            "title": "МТС: отзывы сотрудников о работодателе",
            "contenttype": "organic",
            "breadcrumbs": "https://pravda-sotrudnikov.ru › company › mts-3",
            "stars": "Рейтинг: 2,9",
            "passage": "Минусы конечно есть в работе, это все-таки работа, а не курорт. Но для меня плюсы в работе МТС однозначно перевешивают. Во-первых, зарплата. Все мы работаем ...",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://pravda-sotrudnikov.ru/company/mts-3"
          },
          "3": {
            "url": "https://dreamjob.ru/employers/25946",
            "title": "Работа в МТС - Отзывы сотрудников - Dream Job",
            "contenttype": "organic",
            "breadcrumbs": "https://dreamjob.ru › ... › Отзывы сотрудников ✅",
            "stars": "Рейтинг: 4",
            "passage": "Изучайте отзывы сотрудников о работе в компании МТС. Будьте в курсе условий труда, зарплат, карьерных возможностей благодаря сервису Dream Job.",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://dreamjob.ru/employers/25946"
          },
          "4": {
            "url": "https://career.habr.com/companies/mts/scores",
            "title": "Оценки и отзывы сотрудников о компании МТС за 2023 год",
            "contenttype": "organic",
            "breadcrumbs": "https://career.habr.com › companies › mts › scores",
            "passage": "",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://career.habr.com/companies/mts/scores"
          },
          "5": {
            "url": "https://nahjob.top/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D1%8F/%D0%BC%D1%82%D1%81",
            "title": "Работа в МТС | 448 отзывов сотрудников",
            "contenttype": "organic",
            "breadcrumbs": "https://nahjob.top › Худшие компании ⚫",
            "passage": "Ищете правдивые отзывы о МТС? Изучаете вакансии и зарплаты? Компания МТС, по статистике отзывов сотрудников нашего сайта, не является рекомендуемой к ...",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://nahjob.top/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D1%8F/%D0%BC%D1%82%D1%81"
          },
          "6": {
            "url": "https://otzyvy-mobile.ru/mts/otzyvy-sotrudnikov/",
            "title": "МТС Отзывы Сотрудников",
            "contenttype": "organic",
            "breadcrumbs": "https://otzyvy-mobile.ru › МТС",
            "stars": "Рейтинг: 4,9",
            "passage": "Чтобы заранее определиться, подходит ли вам работа в МТС, рекомендуем ознакомиться с отзывами сотрудников МТС. Почитайте о том, какие условия компания ...",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://otzyvy-mobile.ru/mts/otzyvy-sotrudnikov/"
          },
          "7": {
            "url": "https://tipworker.com/otzyvy-sotrudnikov/mts",
            "title": "МТС ᐈ отзывы сотрудников о работе в компании 2022- ...",
            "contenttype": "organic",
            "breadcrumbs": "https://tipworker.com › otzyvy-sotrudnikov › mts",
            "stars": "Рейтинг: 3,5",
            "passage": "На нашем сайте представлены реальные отзывы сотрудников о работе в компании МТС. Узнайте условия труда, размер зарплаты, плюсы и минусы из отзывов о ...",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://tipworker.com/otzyvy-sotrudnikov/mts"
          },
          "8": {
            "url": "https://govorimorabote.ru/mts-otzyvy-sotrudnikov.html",
            "title": "МТС - отзывы сотрудников",
            "contenttype": "organic",
            "breadcrumbs": "https://govorimorabote.ru › mts-otzyvy-sotrudnikov",
            "passage": "",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://govorimorabote.ru/mts-otzyvy-sotrudnikov.html"
          },
          "9": {
            "url": "https://www.banki.ru/services/official/bank/?ID=6055",
            "title": "Отзывы о работе в банке «МТС Банк",
            "contenttype": "organic",
            "breadcrumbs": "https://www.banki.ru › services › official › bank",
            "passage": "Все нравилось в работе, положительный отзыв проверяется. зарплата соцпакет коллектив руководство общий комфорт. Когда сюда шел устраиваться не думал. что ...",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://www.banki.ru/services/official/bank/?ID=6055"
          },
          "10": {
            "url": "https://spb.rabota.ru/company/mts13/feedback/",
            "title": "Работа в МТС - отзывы сотрудников в Санкт-Петербурге",
            "contenttype": "organic",
            "breadcrumbs": "https://spb.rabota.ru › Каталог компаний",
            "passage": "Отзывы о работе в МТС в Санкт-Петербурге. Узнайте об условиях труда, зарплате, руководстве.",
            "cachelink": "https://webcache.googleusercontent.com/search?q=cache:https://spb.rabota.ru/company/mts13/feedback/"
          }
        }
      }
    }
  })

  export { parser }