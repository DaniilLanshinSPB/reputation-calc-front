import { createRouter, createWebHistory } from 'vue-router'
import MainFormPage from '../views/MainFormPage.vue'
import EnginesPage from '../views/EnginesPage.vue'
import CalculationsPage from '../views/CalculationsPage.vue'

const routes = [
  {
    path: '/calculator/',
    name: 'form',
    component: MainFormPage,
  },
  {
    path: '/calculator/',
    name: 'engines',
    component: EnginesPage
  },
  {
    path: '/calculator/',
    name: 'calculations',
    component: CalculationsPage
  }
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router
