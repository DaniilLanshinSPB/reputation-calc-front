import { defineStore } from 'pinia'

export const usePayloadStore = defineStore('payloadStore', {
  state: () => {
    return {
      payload: {},
      userQuery: '',
      cities: [],
      geoIds: {},
      selectedRegion: {
        name: 'Россия',
        payload: {
          google: '2643',
          yandex: '225'
        }
      },
      pdfPayload: {},
      taskID: null,
      userSteps: [],
      wordstatKeys: {},
      parserPayload: {},
      calculationsPayload: {},
      selectedUserKeywords: [],
      currentPage: 'form',
      mainSearchKeys: [],
      metrikaGoals: {
        startfillingcalc: false,
        choosekeywordcalc: false,
        choosenegativepositioncalc: false,
        clickyeascalc: false,
        clicknocalc: false,
        getreportcalc: false,
        checklinkcalc: false
      },
      needToUpdate: true,
      iframeUrl: ''
    }
  },
  actions: {
    setPayload(payload) {
      this.payload = payload
    },
    setUserQuery(keywords) {
      this.userQuery = keywords
    },
    setCities(cities) {
      this.cities = cities
    },
    setGeoIds(geoIds) {
      this.geoIds = geoIds
    },
    setPdfPayload(payload) {
      this.pdfPayload = payload
    },
    setSelectedRegion(payload) {
      this.selectedRegion = payload
    },
    setTaskID(payload) {
      this.taskID = payload
    },
    setUserSteps(payload) {
      this.userSteps.push(payload)
    },
    setWordstatKeys(payload) {
      this.wordstatKeys = payload
    },
    setParserPayload(payload) {
      this.parserPayload = payload
    },
    setCalculationsPayload(payload) {
      this.calculationsPayload = payload
    },
    deleteUserStep(payload) {
      this.userSteps = payload
    },
    setUserKeywords(payload) {
      this.selectedUserKeywords = payload
    },
    clearUserKeywords(payload) {
      this.selectedUserKeywords = payload
    },
    setCurrentPage(payload) {
      this.currentPage = payload
    },
    setMainSearchKeys(payload) {
      this.mainSearchKeys = payload
    },
    setStartfillingcalc(payload) {
      this.metrikaGoals.startfillingcalc = payload
    },
    setChoosekeywordcalc(payload) {
      this.metrikaGoals.choosekeywordcalc = payload
    },
    setChoosenegativepositioncalc(payload) {
      this.metrikaGoals.choosenegativepositioncalc = payload
    },
    setClickyeascalc(payload) {
      this.metrikaGoals.clickyeascalc = payload
    },
    setClicknocalc(payload) {
      this.metrikaGoals.clicknocalc = payload
    },
    setGetreportcalc(payload) {
      this.metrikaGoals.getreportcalc = payload
    },
    setChecklinkcalc(payload) {
      this.metrikaGoals.checklinkcalc = payload
    },
    setUpdateFlag(payload) {
      this.needToUpdate = payload
    },
    setAttr(payload) {
      this.iframeUrl = payload
    }
  },
  getters: {
    getPayload(state) {
      return state.payload
    },
    getUserQuery(state) {
      return state.userQuery
    },
    getCities(state) {
      return state.cities
    },
    getGeoIds(state) {
      return state.geoIds
    },
    getSelectedRegion(state) {
      return state.defaultCountry
    },
    getPdfPayload(state) {
      return state.pdfPayload
    },
    getUserSteps(state) {
      return state.userSteps
    },
    getWordstatKeys(state) {
      return state.wordstatKeys
    },
    getParserPayload(state) {
      return state.parserPayload
    }
  }
})
