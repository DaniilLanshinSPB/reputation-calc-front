const badUrls = [
  'стаж.рф',                'сотрудников-правда.рф',  'work-info.org',
  'work-info.name',         'whoisyourboss.xyz',      'vseotzyvy.review',
  'vnutri.org',             'tipworker.com',          'stena-otzyvov.ru',
  'sotrydnik.com',          'skagiorabote.ru',        'shefdostal.org',
  'ru.otzyv.com',           'ru-otzyvi.ru',           'reviewscompanies.club',
  'retwork.com',            'reputaciy-otzyvy.com',   'rabotaip.ru',
  'rabotagovno.info',       'rabota.kitabi.ru',       'pravda-sotrudnikov.ru',
  'pravda-sotrudnikov.org', 'otzyvysotrudnikov.club', 'otzyvy.review',
  'otzyvua.net',            'otzyvru.ru',             'otzyvru.com',
  'otzyvi-vse.ru',          'otzyvi-top.ru',          'otzyvi-com.ru',
  'otzyvcom.ru',            'otzyv.com.ru',           'otzyv-com.ru',
  'otzovik.pro',            'otzomir.com',            'otzivisotrudnikov.ru',
  'otzivi-tut.ru',          'otziver.ru',             'otziv-o-rabote.ru',
  'otziv-club.ru',          'orabote.day',            'orabote.xyz',
  'orabote.top',            'orabote.biz',            'ocompanii.net',
  'o-rabote.org',           'nerab.ru',               'ne.orabote.net',
  'nahjob.top',             'nachalnik-m.ru',         'mnenieorabote.ru',
  'mnenie-sotrudnikov.ru',  'klio.top',               'job-yell.ru',
  'bossham.ru',             'black-job.net',          'bework.org',
  'antijob.top',            'antijob.site',           'antijob.net',
  'antijob.name',           'anitjob.name',           'about-job.ru',
  'otzovik.com',            'irecommend.ru',          'otzovik.com',
  'irecommend.ru',          'flamp.ru',               'yell.ru',
  'spr.ru'
]


export {badUrls}
