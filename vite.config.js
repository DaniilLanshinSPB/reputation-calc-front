import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

function makeid(length) {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  let counter = 0;
  while (counter < length) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
    counter += 1;
  }
  return result;
}

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  build: {
    // target: ['es2015'],
    // sourcemap: true,
    rollupOptions: {
      output: {
        chunkFileNames: `preline.${makeid(8)}.min.js`,
        entryFileNames: `calc.${makeid(8)}.min.js`,
        assetFileNames: `calc.${makeid(8)}.min.css`
      }
    },
    outDir: '../crisis_journal/wordpress/root_files/assets/',
    emptyOutDir: false,
  },
})
